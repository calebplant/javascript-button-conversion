import { api, LightningElement, wire } from 'lwc';
import { getRecord} from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import convertRecordType from '@salesforce/apex/ChangeRecordTypeController.convertRecordType';

export default class ChangeRecordType extends LightningElement {
    @api recordId;
    @api sobjectName;
    @api filterValue;

    @wire (getRecord, { recordId: '$recordId', fields: '$fields' })
    getObj(response) {
        if(response.data) {
            console.log('Retrieved from server: ');
            console.log(response.data);
            this.obj = response.data;
            if(!this._obj) {
                this.tryConvertRecordType();
            }
            this._obj = response;
        }
        if(response.error) {
            console.log('Error fetching object!');
            console.log(response.error);
        }
    }

    obj;
    fields;
    _obj;

    connectedCallback() {
        this.fields = [`${this.sobjectName}.${this.filterValue}`];
    }

    tryConvertRecordType() {
        let payload = {
            recordId: this.obj.id,
            recordType: this.obj.recordTypeInfo.name,
            filteringValue: this.obj.fields[this.filterValue].value
        }
        console.log('Payload:');
        console.log(payload);
        convertRecordType(payload)
        .then(result => {
            console.log('Response: ');
            console.log(result);
            this.dispatchEvent(new CustomEvent('refresh'));
        })
        .catch(error => {
            console.log('Error converting record type!');
            console.log(error);
            const evt = new ShowToastEvent({
                title: 'Error Converting REcord',
                message: error,
                variant: 'error',
            });
            this.dispatchEvent(evt);
        })
    }
}