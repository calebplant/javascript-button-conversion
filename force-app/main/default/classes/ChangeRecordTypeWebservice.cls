global with sharing class ChangeRecordTypeWebservice {
    
    Webservice static ChangeResult convertRecordType(Id recordId, String recordType, String filteringValue)
    {
        ChangeResult response = new ChangeResult();
        // Get record's sObject name
        String objTypeName = recordId.getSobjectType().getDescribe().getName();

        // Fetch custom metadata for record's recordtype
        Record_Type_Conversion__mdt conversionSettings = getConversionSettings(objTypeName, recordType);
        if(conversionSettings == null) {
            // Return error if no settings found for that record type
            response.errorMsg = 'No configured conversion settings on this record type!';
            return response;
        }

        // Check if passed filter value is valid in the record type's conversion settings
        Boolean allowUpdate = shouldAllowUpdate(filteringValue, conversionSettings.Allow_Update_Status__c);
        if(allowUpdate) {
            try{
                updateRecordType(recordId, objTypeName, conversionSettings.Ending_Type__c);
                return response;
            } catch(Exception e) {
                System.debug('Error updating record type!');
                System.debug(e.getMessage());
                response.errorMsg = 'Error converting record! ' + e.getMessage();
                return response;
            }
        } else {
            // Return error if filtering value was invalid
            response.errorMsg = filteringValue + ' is invalid for conversion!';
            return response;
        }
        
    }

    private static Record_Type_Conversion__mdt getConversionSettings(String objType, String startRecordType)
    {
        List<Record_Type_Conversion__mdt> settings = [SELECT Id, Starting_Type__c, Ending_Type__c, Allow_Update_Status__c
                                                     FROM Record_Type_Conversion__mdt
                                                     WHERE Object_Type__c = :objType AND Starting_Type__c = :startRecordType];
        return settings.size() > 0 ? settings[0] : null;
    }

    private static Boolean shouldAllowUpdate(String filteringValue, String allowedValuesStr)
    {
        List<String> allowedValues = allowedValuesStr.split(',');
        return allowedValues.contains(filteringValue);
    }

    private static void updateRecordType(Id recordId, String objTypeName, String targetRecordTypeName)
    {
        sObject updatedObj = (SObject) Type.forName(objTypeName).newInstance();
        Id targetRecordTypeId = updatedObj.getsObjectType().getDescribe()
                                            .getRecordTypeInfosByName().get(targetRecordTypeName).getRecordTypeId();
        updatedObj.put('Id', recordId);
        updatedObj.put('RecordTypeId', targetRecordTypeId);
        upsert updatedObj;
    }

    global class ChangeResult{
        webservice String errorMsg;
    }
}
