# Converting a Javascript Button for Lightning Experience (LEX)

This quick guide will hopefully help you with converting your Classic JS Buttons over to LEX. It references a trailhead that should cover most basic cases, and then introduces a more complex example we must implement a custom lightning component for. It also doubles as a bit of a primer for working with Lightning Components.

## Why

JS Buttons played a useful role in Classic Salesforce page interactions, but they pose a serious security risk. Therefore, LEX eliminated them completely. Still, orgs rely on these interactions and therefore need to be updated to accomidate the loss of JS buttons.

## How

Because of the way LEX is structured, there is no one-to-one equivilent feature that matches Classic JS Buttons. Instead, you must evaluate what your JS button does and implement its functionality via an appropriate LEX technology. There is a [trailhead module here](https://trailhead.salesforce.com/en/content/learn/modules/lex_javascript_button_migration) that serves as a good starting point. It gives exmaples for some basic cases, so I will just try to cover a few things the trailhead does not.

## Simpler Cases

The previously mentioned trailhead module covers most basic cases, so I'll just list a few examples of things you can do out-of-the-box with Quick Actions that might cover your use case:

* Create/Update/validate a record
* Send an email
* Link to a Custom Visualforce page
* Launch a flow

## More Interesting Cases

For cases that require more customization, you will want to launch a Lightning Component from a Quick Action. This is what we will cover below.

## The JS Button

First let's take a look at a fairly basic javascript button we will be converting. 

**Function:** The JS button can be placed on any record detail page. When clicked, it will check some custom metadata settings and convert the record's Record Type based on the metadata settings. If the conversion is not valid, it will alert the user. As you see below, the button is really basic and essentially just calls an apex function and passes it some parameters, then either refreshes the page on success or tells the user the error.

### JS Button

```javascript
{!REQUIRESCRIPT("/soap/ajax/30.0/connection.js")}
{!REQUIRESCRIPT("/soap/ajax/24.0/apex.js")} 

var response = sforce.apex.execute("ChangeRecordTypeWebservice","convertRecordType",{recordId:"{!Opportunity.Id}", recordType:"{!Opportunity.RecordType}", filteringValue: "{!Opportunity.StageName}"});

console.log(response[0]);

if(response[0]['errorMsg']) {
alert(response[0]['errorMsg']);
} else {
window.location.reload();
}
```

Now let's look at the apex function that it calls. To understand it, let's take a quick detour and check out the cusotm metadata setting it utilizes.

| Field               | Type     | Example Value       | Explaination                                        |
|---------------------|----------|---------------------|-----------------------------------------------------|
| Object Type         | Picklist | Opportunity         | Record's sobject's name                             |
| Starting Type       | Picklist | Support Opportunity | Record's current Record Type name                   |
| Ending Type         | Picklist | Sales Opportunity   | Record Type name we are converting to               |
| Allow Update Status | Text     | New,Closed Lost     | Comma-separated list of valid values for validation |
| Allow Update Field  | Text     | StageName           | Field name we will use for validation               |

For example: we press the button on an Opportunity record detail page whose Record Type is Support Opportunity. It will try to convert it to a Sales Opportunity. The conversion will succeed if the record's StageName is set to "New" or "Closed Lost".

### JS Button Backend

```java
global with sharing class ChangeRecordTypeWebservice {
    
    Webservice static ChangeResult convertRecordType(Id recordId, String recordType, String filteringValue)
    {
        ChangeResult response = new ChangeResult();
        // Get record's sObject name
        String objTypeName = recordId.getSobjectType().getDescribe().getName();

        // Fetch custom metadata for record's recordtype
        Record_Type_Conversion__mdt conversionSettings = getConversionSettings(objTypeName, recordType);
        if(conversionSettings == null) {
            // Return error if no settings found for that record type
            response.errorMsg = 'No configured conversion settings on this record type!';
            return response;
        }

        // Check if passed filter value is valid in the record type's conversion settings
        Boolean allowUpdate = shouldAllowUpdate(filteringValue, conversionSettings.Allow_Update_Status__c);
        if(allowUpdate) {
            try{
                updateRecordType(recordId, objTypeName, conversionSettings.Ending_Type__c);
                return response;
            } catch(Exception e) {
                System.debug('Error updating record type!');
                System.debug(e.getMessage());
                response.errorMsg = 'Error converting record! ' + e.getMessage();
                return response;
            }
        } else {
            // Return error if filtering value was invalid
            response.errorMsg = filteringValue + ' is invalid for conversion!';
            return response;
        }
        
    }

    private static Record_Type_Conversion__mdt getConversionSettings(String objType, String startRecordType)
    {
        List<Record_Type_Conversion__mdt> settings = [SELECT Id, Starting_Type__c, Ending_Type__c, Allow_Update_Status__c
                                                     FROM Record_Type_Conversion__mdt
                                                     WHERE Object_Type__c = :objType AND Starting_Type__c = :startRecordType];
        return settings.size() > 0 ? settings[0] : null;
    }

    private static Boolean shouldAllowUpdate(String filteringValue, String allowedValuesStr)
    {
        List<String> allowedValues = allowedValuesStr.split(',');
        return allowedValues.contains(filteringValue);
    }

    private static void updateRecordType(Id recordId, String objTypeName, String targetRecordTypeName)
    {
        sObject updatedObj = (SObject) Type.forName(objTypeName).newInstance();
        Id targetRecordTypeId = updatedObj.getsObjectType().getDescribe()
                                            .getRecordTypeInfosByName().get(targetRecordTypeName).getRecordTypeId();
        updatedObj.put('Id', recordId);
        updatedObj.put('RecordTypeId', targetRecordTypeId);
        upsert updatedObj;
    }

    global class ChangeResult{
        webservice String errorMsg;
    }
}

```

## The Conversion Process

How do we translate this button into a Lightning Component? Let's take it step by step.

1. Put a button on the screen for users to click
1. Get the record's details so we can pass them to an Apex function
1. Call the apex function
1. React to the server's response

**Important Note**: Lightning Components only support the [Lightning Data Service API](https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/data_service.htm). If your JS button relied on other APIs, you will likely have to call those APIs from an Apex controller function instead of directly from Javascript.

### Making the Button

First let's expose a Lightning Component as a Quick Action. Because SF is trying to move towards LWC, let's make our component in LWC. Unfortunately, LWC cannot directly be called via Quick Action yet. Therefore, we will create an Aura Component that contains our LWC.

To expose our Aura Component we use the [force:lightningQuickActionWithoutHeader interface](https://developer.salesforce.com/docs/component-library/bundle/force:lightningQuickActionWithoutHeader/documentation).

```html
<aura:component implements="force:lightningQuickActionWithoutHeader">

    <c:changeRecordType> 
    </c:changeRecordType>
</aura:component>	
```

Now we can select our Lightning Component when setting up a Quick Action.

![Selecting our new Lightning Component](media/selecting-cmp.png)

### Get Record Details

In the JS button, we were able to directly use merge fields to get the record's values. Ex: ```"{!Opportunity.StageName}"```. In our Lightning Component, we only have direct access to (among other things) the record Id and sobject type via the [force:hasRecordId](https://developer.salesforce.com/docs/component-library/bundle/force:hasRecordId/documentation) and [force:hasSObjectName](https://developer.salesforce.com/docs/component-library/bundle/force:hasSObjectName/documentation) interfaces. We can add these to our Aura component and then pass them to our child LWC. Let's also pass the name of the field we will use for validation later.

```html
<aura:component implements="force:lightningQuickActionWithoutHeader,force:hasRecordId,force:hasSObjectName">

    <c:changeRecordType
        recordId="{!v.recordId}"
        sobjectName="{!v.sObjectName}"
        filterValue="StageName">
    </c:changeRecordType>
</aura:component>	
```

```js
import { api, LightningElement } from 'lwc';

export default class ChangeRecordType extends LightningElement {
    @api recordId;
    @api sobjectName;
    @api filterValue;
}
```

With these values, our LWC can use the [getRecord Api](https://developer.salesforce.com/docs/component-library/documentation/en/lwc/reference_wire_adapters_record) to pull field values for the record. In this case, the only field we need to pull is the one we will be using for validation (StageName in our example).

```js
import { api, LightningElement, wire } from 'lwc';
import { getRecord} from 'lightning/uiRecordApi';

export default class ChangeRecordType extends LightningElement {
    @api recordId;
    @api sobjectName;
    @api filterValue;

    obj;
    fields;
    _obj;

    @wire (getRecord, { recordId: '$recordId', fields: '$fields' })
    getObj(response) {
        if(response.data) {
            this.obj = response.data;
            if(!this._obj) {
                // Do conversion here
            }
            this._obj = response;
        }
        if(response.error) {
            console.log('Error fetching object!');
            console.log(response.error);
        }
    }

    connectedCallback() {
        this.fields = [`${this.sobjectName}.${this.filterValue}`];
    }
}
```

### Call the Apex Function

Now that we have our record's values, we need to pass them to the backend. [Review how to do so here](https://developer.salesforce.com/docs/component-library/documentation/en/lwc/lwc.apex).

First we need to recreate our Apex class as a "controller" instead of a webservice. Just copy-paste the old class and make 3 easy changes:

1. **Change class declaration**

Change
```java
global with sharing class ChangeRecordTypeWebservice {
    ...
}
```
to
```java
public with sharing class ChangeRecordTypeController {
    ...
}
```
2. **Annotate called function**

Change
```java
Webservice static ChangeResult convertRecordType(Id recordId, String recordType, String filteringValue)
    {
        ...
    }
```
to
```java
@AuraEnabled
    public static ChangeResult convertRecordType(Id recordId, String recordType, String filteringValue)
    {
        ...
    }
```
3. **Annotate returned Object values**

Change
```java
global class ChangeResult{
        webservice String errorMsg;
    }
```
to
```java
public class ChangeResult{
        @AuraEnabled
        public String errorMsg {get; set;}
    }
```

That's it for the backend. To call the function from the LWC, import it at the top of the file and then let's add a function to call it and eventually handle the response:

```js
import { api, LightningElement, wire } from 'lwc';
import { getRecord} from 'lightning/uiRecordApi';
import convertRecordType from '@salesforce/apex/ChangeRecordTypeController.convertRecordType';

export default class ChangeRecordType extends LightningElement {
    @api recordId;
    @api sobjectName;
    @api filterValue;

    obj;
    fields;
    _obj;

    @wire (getRecord, { recordId: '$recordId', fields: '$fields' })
    getObj(response) {
        if(response.data) {
            this.obj = response.data;
            if(!this._obj) {
                this.tryConvertRecordType();
            }
            this._obj = response;
        }
        if(response.error) {
            console.log('Error fetching object!');
            console.log(response.error);
        }
    }

    connectedCallback() {
        this.fields = [`${this.sobjectName}.${this.filterValue}`];
    }

    tryConvertRecordType() {
        let payload = {
            recordId: this.obj.id,
            recordType: this.obj.recordTypeInfo.name,
            filteringValue: this.obj.fields[this.filterValue].value
        }
        convertRecordType(payload)
        .then(result => {
            // Handle successful server response
        })
        .catch(error => {
            // Handler error server response
        });
    }
}
```

## Handling the Server Response

Finally, let's implement how the button will react to the server's response. If the server returns an error message, it should display to the user. If no error message is returned, it should refresh the record's page.

To refresh the record's page, we have a out-of-the-box function we can call from the Aura controller: [e.force:closeQuickAction](https://developer.salesforce.com/docs/component-library/bundle/force:closeQuickAction/documentation). We can call it from our LWC by dispatching an event (we call it "refresh" in the below code) that is caught and handled in the Aura controller.

To display an error message to the user, we have a out-of-the-box [toast message function](https://developer.salesforce.com/docs/component-library/bundle/lightning-platform-show-toast-event/documentation) we can call directly from our LWC. The completed code is found below.

### ChangeRecordTypeWrapper.cmp
```html
<aura:component implements="force:lightningQuickActionWithoutHeader,force:hasRecordId,force:hasSObjectName">

    <c:changeRecordType
        recordId="{!v.recordId}"
        sobjectName="{!v.sObjectName}"
        filterValue="StageName"
        onrefresh="{!c.refresh}"> 
    </c:changeRecordType>
</aura:component>	
```

#### ChangeRecordTypeWrapperController.js
```js
({
    refresh : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
	}
})

```

### ChangeRecordTypeWrapper.js
```js
import { api, LightningElement, wire } from 'lwc';
import { getRecord} from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import convertRecordType from '@salesforce/apex/ChangeRecordTypeController.convertRecordType';

export default class ChangeRecordType extends LightningElement {
    @api recordId;
    @api sobjectName;
    @api filterValue;

    obj;
    fields;
    _obj;

    @wire (getRecord, { recordId: '$recordId', fields: '$fields' })
    getObj(response) {
        if(response.data) {
            this.obj = response.data;
            if(!this._obj) {
                this.tryConvertRecordType();
            }
            this._obj = response;
        }
        if(response.error) {
            console.log('Error fetching object!');
            console.log(response.error);
        }
    }

    connectedCallback() {
        this.fields = [`${this.sobjectName}.${this.filterValue}`];
    }

    tryConvertRecordType() {
        let payload = {
            recordId: this.obj.id,
            recordType: this.obj.recordTypeInfo.name,
            filteringValue: this.obj.fields[this.filterValue].value
        }
        convertRecordType(payload)
        .then(result => {
            console.log('Response: ');
            console.log(result);
            this.dispatchEvent(new CustomEvent('refresh'));
        })
        .catch(error => {
            console.log('Error converting record type!');
            console.log(error);
            const evt = new ShowToastEvent({
                title: 'Error Converting REcord',
                message: error,
                variant: 'error',
            });
            this.dispatchEvent(evt);
        });
    }
}
```

## Conclusion

Converting JS buttons for LEX is not a straight forward process, but hopefully this guide has made it a bit easier to follow. For simple solutions, built-in Quick Action solutions can make the process a breeze. For complex, custom implementations, this guide has shown how we can launch a Lightning Component via Quick Action, get record values, pass them to the backend, and react to the server's response.